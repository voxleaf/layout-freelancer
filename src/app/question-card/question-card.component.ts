import { Component, OnInit , Input, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { Question } from "../models/question";

@Component({
  selector: 'app-question-card',
  templateUrl: './question-card.component.html',
  styleUrls: ['./question-card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class QuestionCardComponent implements OnInit {

  @Input('question-card') question:Question;
  @Output('selected-question-card') selectedQuestionCard = new EventEmitter<Question>();
  selected : boolean = false;

  constructor() { }

  ngOnInit() {
  }

  onSelect(){
    console.log('inside card',this.question);
    this.selected=!this.selected;
    if(this.selected){
      this.selectedQuestionCard.emit(this.question);
    }
  }
}
