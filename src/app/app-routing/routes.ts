import { Routes } from '@angular/router';

import { DashboardComponent } from '../dashboard/dashboard.component';
import { ProfileComponent } from '../profile/profile.component';
import { NotificationComponent } from '../notification/notification.component';
import { SignupComponent  } from '../signup/signup.component';
import { WorkComponent } from '../work/work.component';
import { QuestionsComponent } from '../questions/questions.component';

export const routes: Routes = [
  { path: 'dashboard',      component: DashboardComponent, data: { showSidebar: false, method: 'dashboard' } },
  { path: 'profile',        component: ProfileComponent },
  { path: 'signup',         component: SignupComponent, data: { showSidebar: false , method: 'signup'} },
  { path: 'notification',   component: NotificationComponent },
  { path: 'questions',      component: QuestionsComponent },
  { path: 'work',           component: WorkComponent },
  
  { path: '', redirectTo:   '/dashboard', pathMatch: 'full' }
];