
export class SubQuestion{
    id          : string;
    name        : string;
    url         : string;
    icon?       : string;
    description : string;
}