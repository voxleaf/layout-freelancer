import { AuthMethod } from './authMethod';

export const AUTHMETHODS: AuthMethod[] = [
    {
        id: '0',
        name: 'facebook',
        icon: 'fa-facebook-f',
        enable: true,
        // tslint:disable-next-line:max-line-length
    },
    {
        id: '1',
        name: 'twitter',
        icon: 'fa-twitter',
        enable: true,
        // tslint:disable-next-line:max-line-length
    },
    {
        id: '2',
        name: 'google',
        icon: 'fa-google-plus-g',
        enable: true,
        // tslint:disable-next-line:max-line-length
    },
    
    {
        id: '3',
        name: 'linkedin',
        icon: 'fa-linkedin-in',
        enable: true,
        // tslint:disable-next-line:max-line-length
    },
    {
        id: '4',
        name: 'twitter',
        icon: 'fa-twitter',
        enable: true,
        // tslint:disable-next-line:max-line-length
    },
    {
        id: '5',
        name: 'instagram',
        icon: 'fa-instagram',
        enable: true,
        // tslint:disable-next-line:max-line-length
    },
    
    
];