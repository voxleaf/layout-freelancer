import { Question, Technology } from "./question";


export const TECHNOLOGY: Technology[]= [
    {
        id: '0',
        code :'design',
        heading: 'About you project?',
        name: 'Design',
        url : '/assets/images/questions/frontend.png',
        category: [
            {
                id: '0',
                heading: 'About you project?',
                name: 'Front End',
                url : '/assets/images/questions/frontend.png',
                description: 'Letest front end tools for your project.',
                sub: [
                    {
                        id: '0',
                        name: 'HTML/CSS',
                        url: '/assets/images/questions/html-css.png',
                        description: 'HTML/CSS, Bootstrap or Material',
                    },
                    {
                        id: '1',
                        name: 'Angular',
                        url: '/assets/images/questions/angular.png',
                        description: 'Angular is powerfull framework',
                    },
                    {
                        id: '2',
                        name: 'React',
                        url: '/assets/images/questions/react.png',
                        description: 'React helps you to get done your project quick',
                    },
                    {
                        id: '3',
                        name: 'ionic',
                        url: '/assets/images/questions/ionic.png',
                        description: 'Hybrid Mobile Applications with ionic',
                    },
                ]
            }
        ]
       
    },
    {
        id      : '1',
        code    : 'development' ,
        heading : 'Languages we speak',
        name    : 'Development',
        url         : '/assets/images/questions/development.png',
        category: [
            {
                id          : '1',
                heading     : 'Which backend technology your required?',
                name        : 'Back End',
                url         : '/assets/images/questions/backend.png',
                description : 'We can develop this for you',
                sub: [
                    {
                        id: '0',
                        name: 'PHP',
                        url: '/assets/images/questions/php.png',
                        description: 'PHP, Bootstrap or Material',
                    },
                    {
                        id: '1',
                        name: 'CodeIgnitor',
                        url: '/assets/images/questions/codeignitor.png',
                        description: 'Angular is powerfull framework',
                    },
                    {
                        id: '2',
                        name: 'node.js',
                        url: '/assets/images/questions/node.png',
                        description: 'React helps you to get done your project quick',
                    },
                    {
                        id: '3',
                        name: 'REST API',
                        url: '/assets/images/questions/api.png',
                        description: 'Hybrid Mobile Applications with ionic',
                    },
                ]
            }
        ] 
    },
    {
        id      : '2',
        code    : 'fullstack' ,
        heading : 'Languages we speak',
        url     : '/assets/images/questions/fullstack.png',
        name    : 'Full Stack',
        category: [
            {
                id          : '0',
                heading     : 'About you project?',
                name        : 'Front End',
                url         : '/assets/images/questions/frontend.png',
                description : 'Letest front end tools for your project.',
                sub: [
                    {
                        id: '0',
                        name: 'HTML/CSS',
                        url: '/assets/images/questions/html-css.png',
                        description: 'HTML/CSS, Bootstrap or Material',
                    },
                    {
                        id: '1',
                        name: 'Angular',
                        url: '/assets/images/questions/angular.png',
                        description: 'Angular is powerfull framework',
                    },
                    {
                        id: '2',
                        name: 'React',
                        url: '/assets/images/questions/react.png',
                        description: 'React helps you to get done your project quick',
                    },
                    {
                        id: '3',
                        name: 'ionic',
                        url: '/assets/images/questions/ionic.png',
                        description: 'Hybrid Mobile Applications with ionic',
                    },
                ]
            },
            {
                id          : '1',
                heading     : 'Which backend technology your required?',
                name        : 'Back End',
                url         : '/assets/images/questions/backend.png',
                description : 'We can develop this for you',
                sub: [
                    {
                        id: '0',
                        name: 'PHP',
                        url: '/assets/images/questions/php.png',
                        description: 'PHP, Bootstrap or Material',
                    },
                    {
                        id: '1',
                        name: 'CodeIgnitor',
                        url: '/assets/images/questions/codeignitor.png',
                        description: 'Angular is powerfull framework',
                    },
                    {
                        id: '2',
                        name: 'node.js',
                        url: '/assets/images/questions/node.png',
                        description: 'React helps you to get done your project quick',
                    },
                    {
                        id: '3',
                        name: 'REST API',
                        url: '/assets/images/questions/api.png',
                        description: 'Hybrid Mobile Applications with ionic',
                    },
                ]
            },
            {
                id          : '2',
                heading     : 'Databases we offer',
                name        : 'Database',
                url         : '/assets/images/questions/dynomo.png',
                description : 'We can develop this for you',
                sub: [
                        {
                            id: '0',
                            name: 'MongoDB',
                            url: '/assets/images/questions/mongodb.png',
                            description: 'PHP, Bootstrap or Material',
                        },
                        {
                            id: '1',
                            name: 'FIREBASE',
                            url: '/assets/images/questions/firebase.png',
                            description: 'Angular is powerfull framework',
                        },
                        {
                            id: '2',
                            name: 'MySQL',
                            url: '/assets/images/questions/mysql.png',
                            description: 'React helps you to get done your project quick',
                        },
                        {
                            id: '3',
                            name: 'DynomoDB',
                            url: '/assets/images/questions/dynomo.png',
                            description: 'React helps you to get done your project quick',
                        },
                    ]
            },
        ] 
    },
    {
        id      : '3',
        code    : 'automation' ,
        heading : 'Automation',
        name    : 'CI/CD',
        url : '/assets/images/questions/ci-cd.png',

        category: [
            {
                id: '0',
                heading: 'Continuous Integration and Continuous Delivery',
                name: 'CI/CD',
                url : '/assets/images/questions/ci-cd.png',
                description: 'We can develop this for you',
                sub: [
                        {
                            id: '0',
                            name: 'GIT',
                            url: '/assets/images/questions/git.png',
                            description: 'GIT',
                        },
                        {
                            id: '1',
                            name: 'Docker',
                            url: '/assets/images/questions/docker.png',
                            description: 'Angular is powerfull framework',
                        },
                        {
                            id: '2',
                            name: 'Jenkins',
                            url: '/assets/images/questions/jenkins.png',
                            description: 'React helps you to get done your project quick',
                        },
                        {
                            id: '3',
                            name: 'Ansible',
                            url: '/assets/images/questions/ansible.png',
                            description: 'Hybrid Mobile Applications with ionic',
                        },    
                    ]
            },
            {
                id: '1',
                heading: 'Cloud Infrastructer',
                name: 'Cloud',
                url : '/assets/images/questions/ci-cd.png',
                description: 'We can develop this for you',
                sub: [
                        {
                            id: '0',
                            name: 'Amazon AWS',
                            url: '/assets/images/questions/aws.png',
                            description: 'Amazon AWS',
                        },
                        {
                            id: '1',
                            name: 'Microsoft Azure',
                            url: '/assets/images/questions/azure.png',
                            description: 'Microsft Azure Cloud Services',
                        },
                        {
                            id: '2',
                            name: 'Google Cloud',
                            url: '/assets/images/questions/googlecloud.png',
                            description: 'Google Cloud for app and web',
                        },
                        {
                            id: '3',
                            name: 'Linode',
                            url: '/assets/images/questions/linode.png',
                            description: 'Linode is available cheapest cloud in market',
                        },    
                    ]
            }
        ] 
    },
    
];