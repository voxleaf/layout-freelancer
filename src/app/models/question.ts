import { SubQuestion } from "./subQuestions";

export class Question{
    id          : string;
    heading     : string;
    name        : string;
    url         : string;
    description : string;
    sub         : SubQuestion[];
}

export class Technology{
    id          : string;
    code        : string;
    name        : string;
    heading     : string;
    url         : string;
    description?:string;
    category    : Question[];
}

