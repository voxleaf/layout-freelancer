import { Component, OnInit, ViewChild, ViewEncapsulation, ChangeDetectorRef } from '@angular/core';
import { QuestionService } from '../services/question.service';
import { Question } from '../models/question';
import { Observable, of  } from 'rxjs';
import { map } from 'rxjs/operators';

import { MatStepper } from '@angular/material';
import { Technology } from '../models/question';
import { SubQuestion } from '../models/subQuestions';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss'],
  encapsulation: ViewEncapsulation.None,

})
export class QuestionsComponent implements OnInit {
 
  questions$: Observable<Technology[]>;
  questionSelected = false;
  check = true;
  technology=[];
  @ViewChild('stepper') stepper: MatStepper;
  totalSteps;
 
  constructor(private questionService:QuestionService, private changeDetector: ChangeDetectorRef) { 
    this.questions$ = this.questionService.getQuestions();
    this.questions$.subscribe(tech=>{
      this.totalSteps = this.getArrayWithNumbers(1);
      let techno = Array();
      techno[0] = tech;
      this.technology = techno;
    });
  }

  ngOnInit() {
    
  }


  ngAfterViewChecked(){
    
    if(this.questionSelected){
      console.log(this.questionSelected);
      this.stepper.next();  
      this.questionSelected = false;
    }
    this.changeDetector.detectChanges();
  }

   onSelect(event:Technology){
    this.questionSelected = true;
     if(this.check){
      var val = [];
      val[0]= this.technology[0];
      if(event.category){
        for(let i=1; i <= event.category.length; i++)
          val[i]= event.category[i-1].sub;
        this.check= false;
      }
      this.technology = val;
  }
}
  
  
  getArrayWithNumbers(n) {
      var arr = Array.apply(null, Array(n));
      return arr.map(function (x, i) { return i });
  }

  
}
