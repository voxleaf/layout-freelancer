import { Injectable } from '@angular/core';
import { AuthMethod } from '../models/authMethod';
import { AUTHMETHODS } from '../models/authMethods';

import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }

  getSignupMethods(): Observable<AuthMethod[]>{
    return of(AUTHMETHODS).pipe(delay(500));
  }

}
