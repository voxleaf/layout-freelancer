import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { TECHNOLOGY } from '../models/questions';
import { Question, Technology } from "../models/question";

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  constructor() { }


  getQuestions(): Observable<Technology[]>{
    return of(TECHNOLOGY).pipe(delay(500));
  }

}
