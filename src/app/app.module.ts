import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatStepperModule} from '@angular/material/stepper';
import {MatCardModule} from '@angular/material/card';

import { AppComponent } from './app.component';

import 'hammerjs';
import { SignupComponent } from './signup/signup.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { NotificationComponent } from './notification/notification.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { HeaderComponent } from './header/header.component';
import { AppRoutingModule } from './app-routing/app-routing.module';
import { WorkComponent } from './work/work.component';
import { QuestionsComponent } from './questions/questions.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import { AuthService } from './services/auth.service';
import { QuestionService } from './services/question.service';
import { QuestionCardComponent } from './question-card/question-card.component';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';
import { KeysPipe } from './keys.pipe';

@NgModule({
  declarations: [
    AppComponent,
    SignupComponent,
    DashboardComponent,
    ProfileComponent,
    NotificationComponent,
    SideNavComponent,
    HeaderComponent,
    WorkComponent,
    QuestionsComponent,
    QuestionCardComponent,
    KeysPipe,
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatStepperModule,
    MatCardModule,
    MatButtonToggleModule,
    AppRoutingModule,
    MatButtonModule,
    MatTooltipModule

  ],
  providers: [AuthService, QuestionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
