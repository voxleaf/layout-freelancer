import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { AuthMethod } from '../models/authMethod';

@Component({
  selector: 'signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  methods: AuthMethod[];

  constructor(private authService:AuthService) { }

  ngOnInit() {
    this.authService.getSignupMethods().subscribe(data=>{
      this.methods=data;
      console.log(this.methods);
    }
    );
  }

}
